package com.example.findlocation.ui

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.findlocation.App
import com.example.findlocation.R
import com.example.findlocation.di.MainViewModelProviderFactory
import com.example.findlocation.di.component.DaggerMainActivityComponent
import com.example.findlocation.di.scope.MainScope
import com.example.findlocation.ui.receiver.GpsBroadcastReceiver
import com.example.findlocation.utils.GPS_DIALOG_MESSAGE
import com.example.findlocation.utils.PERMISSIONS_REQUEST_ENABLE_GPS
import com.example.findlocation.utils.YES
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

@MainScope
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var mainViewModelProvider: MainViewModelProviderFactory

    private lateinit var viewModel: MainViewModel

    var gpsBroadcastReceiver: GpsBroadcastReceiver = GpsBroadcastReceiver(object :
        GpsBroadcastReceiver.onGpsBroadcastReceiverListener {
        override fun onGpsStatusChange() {
            viewModel.onGpsEnabled(checkGpsStatus())
        }
    })

    override fun onStart() {
        super.onStart()
        val intentFilter = IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION)
        registerReceiver(gpsBroadcastReceiver, intentFilter)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DaggerMainActivityComponent.builder()
            .appComponent((applicationContext as App).appComponent)
            .build()
            .inject(this)

        viewModel = ViewModelProvider(this, mainViewModelProvider)[MainViewModel::class.java]

        viewModel.checkPermission.observe(this, Observer {
            checkPermission()
        })


        viewModel.showDialog.observe(this, Observer {
            showGpsEnableDialog()
        })

        viewModel.enableGps.observe(this, Observer {
            if (!checkGpsStatus()) {
                showGpsEnableDialog()
            } else {
                viewModel.onGpsEnabled(checkGpsStatus())
            }
        })

        viewModel.locationResult.observe(this, Observer {
            tvLatitude.text = it.latitude.toString()
            tvLongitude.text = it.longitude.toString()

        })

        viewModel.errorMessage.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })

        viewModel.permissionError.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })
        viewModel.init()
    }

    private fun checkPermission() {
            Dexter.withActivity(this)
                .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        viewModel.onPermissionsChecked(report)
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: List<PermissionRequest>,
                        token: PermissionToken
                    ) {
                        token.continuePermissionRequest()
                    }
                })
                .onSameThread()
                .check()
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(gpsBroadcastReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(gpsBroadcastReceiver)
    }

    fun checkGpsStatus(): Boolean {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    private fun showGpsEnableDialog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.apply {
            setMessage(GPS_DIALOG_MESSAGE)
            setCancelable(false)
            setPositiveButton(YES) { _, _ ->
                val enableGpsIntent = Intent(
                    Settings.ACTION_LOCATION_SOURCE_SETTINGS
                )
                startActivityForResult(enableGpsIntent, PERMISSIONS_REQUEST_ENABLE_GPS)
            }
        }
        val alert: AlertDialog = builder.create()
        alert.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            PERMISSIONS_REQUEST_ENABLE_GPS -> {
                viewModel.onGpsEnabled(checkGpsStatus())

            }
        }
    }
}
