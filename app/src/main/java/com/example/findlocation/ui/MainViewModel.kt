package com.example.findlocation.ui

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.findlocation.data.entity.Location
import com.example.findlocation.domin.LocationUseCase
import com.example.findlocation.services.LocationService
import com.example.findlocation.utils.*
import com.karumi.dexter.MultiplePermissionsReport
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class MainViewModel @Inject constructor(
    private val locationUseCase: LocationUseCase,
    val context: Context
) : ViewModel() {

    val compositeDisposable = CompositeDisposable()
    var checkPermission = MutableLiveData<Boolean>()
    var enableGps = MutableLiveData<Boolean>()
    var permissionError = MutableLiveData<String>()
    var errorMessage = MutableLiveData<String>()
    var locationResult = MutableLiveData<Location>()
    var showDialog = MutableLiveData<String>()

    fun init() {
        checkPermission.value = true
        if (isLocationServiceRunning(context) ){
            stopLocationService()
        }
    }


    fun onPermissionsChecked(report: MultiplePermissionsReport) {
        if (report.areAllPermissionsGranted()) {
            enableGps.value = true
        } else if (report.isAnyPermissionPermanentlyDenied) {
        //    permissionError.value = PERMISSION_MANUALLY
        } else {
            errorMessage.value = PERMISSION_ERROR
        }
    }

    fun checkGpsStatus(): Boolean {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }


    fun onGpsEnabled(status: Boolean) {
        if (status) {
            locationUseCase.execute()
                .subscribe({
                    locationResult.value = it
                }, {
                })
                .run { compositeDisposable.add(this) }
        } else {
            compositeDisposable.clear()
            showDialog.value = SHOW_DIALOG
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
        if (checkGpsStatus()) {
            startLocationService(context)
        }
    }


    fun startLocationService(context: Context) {
        if (!isLocationServiceRunning(context)) {
            val serviceIntent = Intent(context, LocationService::class.java)
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                context.startForegroundService(serviceIntent)
            } else {
                context.startService(serviceIntent)
            }
        }
    }

    fun stopLocationService(){
        val serviceIntent = Intent(context, LocationService::class.java)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            context.stopService(serviceIntent)
        } else {
            context.stopService(serviceIntent)
        }
    }


    private fun isLocationServiceRunning(activity: Context): Boolean {
        val manager =
            activity.getSystemService(AppCompatActivity.ACTIVITY_SERVICE) as ActivityManager
        @Suppress("DEPRECATION")
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (LOCATION_SERVICE == service.service.className) {
                return true
            }
        }
        return false
    }

}