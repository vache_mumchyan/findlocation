package com.example.findlocation.ui.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class GpsBroadcastReceiver(private val listener: onGpsBroadcastReceiverListener) : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
         listener.onGpsStatusChange()
    }

    interface onGpsBroadcastReceiverListener {
        fun onGpsStatusChange()

    }
}