package com.example.findlocation.utils


const val BASE_URL = "https://www.google.ru/"
const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 9002
const val PERMISSIONS_REQUEST_ENABLE_GPS = 9003
const val GPS_DIALOG_MESSAGE = "This application requires GPS to work properly, do you want to enable it?"
const val YES = "YES"
const val LOCATION_SERVICE = "com.example.findlocation.services.LocationService"
const val SHOW_DIALOG = "show dialog"
const val PERMISSION_ERROR = "Please set permission"
const val SENDING_LOCATION = "Sending Location to Server"

