package com.example.findlocation.data.repository.remote

import com.example.findlocation.data.entity.Location
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.ResponseBody
import javax.inject.Inject

class RemoteRepository @Inject constructor(private  val locationApi: LocationApi) {
    fun setLocation(location: Location): Single<ResponseBody>  = locationApi.setLocation()

}