package com.example.findlocation.data.repository

import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import androidx.core.content.ContextCompat.getSystemService
import com.example.findlocation.data.entity.Location
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.*
import io.reactivex.Single
import io.reactivex.subjects.SingleSubject
import javax.inject.Inject

class LocationRepository @Inject constructor(private val context: Context) {

    var fusedLocationClient: FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(context.applicationContext)

    private var locationManager: LocationManager? = null

    fun getLocation(): Single<Location> = Single.create { emitter ->

        val available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context)
        if (available == ConnectionResult.SUCCESS) {
            fusedLocationClient.lastLocation
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val location = task.result
                        val l = location?.latitude
                        val geoPoint = Location(
                            l ?: 0.toDouble(),
                            location?.longitude ?: 0.toDouble()
                        )
                        emitter.onSuccess(geoPoint)
                    }
                }
        } else {
            locationManager = context.getSystemService(LOCATION_SERVICE) as LocationManager?;
            val locationListener: LocationListener = object : LocationListener {
                override fun onLocationChanged(p0: android.location.Location?) {
                    val geoPoint = p0?.latitude?.let { Location(it, p0?.longitude) }
                    geoPoint?.let { emitter.onSuccess(it) }
                }
                override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
                }
                override fun onProviderEnabled(provider: String) {
                }
                override fun onProviderDisabled(provider: String) {
                }
            }
            try {
                locationManager?.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    0L,
                    0f,
                    locationListener
                );
            } catch (ex: SecurityException) {
//                Log.d("myTag", "Security Exception, no location available");
            }
        }
    }
}
