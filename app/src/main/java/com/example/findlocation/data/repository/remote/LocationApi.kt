package com.example.findlocation.data.repository.remote

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST

interface LocationApi {

    @GET(".")
    fun setLocation(): Single<ResponseBody>
}