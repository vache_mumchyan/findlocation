package com.example.findlocation.data.entity

data class Location(var latitude: Double, var longitude: Double)