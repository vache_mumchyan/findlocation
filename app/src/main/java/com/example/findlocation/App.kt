package com.example.findlocation

import android.app.Application
import android.content.Context
import com.example.findlocation.di.component.AppComponent
import com.example.findlocation.di.component.DaggerAppComponent
import com.example.findlocation.di.module.AppModule
import javax.inject.Inject

class App : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .build()
    }
}