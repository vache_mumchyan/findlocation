package com.example.findlocation.services

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import com.example.findlocation.App
import com.example.findlocation.R
import com.example.findlocation.di.component.DaggerLocationServiceComponent
import com.example.findlocation.di.scope.ServiceScope
import com.example.findlocation.domin.LocationUseCase
import com.example.findlocation.ui.MainActivity
import com.example.findlocation.utils.SENDING_LOCATION
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


@ServiceScope
class LocationService : Service() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    @Inject
    lateinit var locationUseCase: LocationUseCase
    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder
    private val channellId = "com.example.findlocation.services.exa"
    private val description = "Test notification"

    override fun onBind(p0: Intent?): IBinder? = null

    override fun onCreate() {
        super.onCreate()
        DaggerLocationServiceComponent.builder()
            .appComponent((applicationContext as App).appComponent)
            .build()
            .inject(this)

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE)as NotificationManager
        val pending = PendingIntent.getActivity(applicationContext, 1222, Intent(applicationContext, MainActivity::class.java), 0)

        if (Build.VERSION.SDK_INT >=26) {
            notificationChannel = NotificationChannel(channellId, description, NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(notificationChannel)

            builder = Notification.Builder(applicationContext,channellId)
                .setContentText(SENDING_LOCATION)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pending)
        }else{
            builder = Notification.Builder(applicationContext)
                .setContentText(SENDING_LOCATION)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pending)
        }
            startForeground(1234567,builder.build())
    }
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        locationUseCase.execute()
            .subscribe({
            }, {
            })
            .run { compositeDisposable.add(this) }

        return START_NOT_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}