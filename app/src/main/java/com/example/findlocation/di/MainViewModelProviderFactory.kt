package com.example.findlocation.di

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.findlocation.data.repository.LocationRepository
import com.example.findlocation.data.repository.remote.RemoteRepository
import com.example.findlocation.domin.LocationUseCase
import com.example.findlocation.ui.MainViewModel
import javax.inject.Inject

class MainViewModelProviderFactory @Inject constructor(
    private val locationUseCase: LocationUseCase,
    private val context: Context
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(MainViewModel::class.java!!)) {
            MainViewModel(locationUseCase,context) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }

}