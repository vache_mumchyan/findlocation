package com.example.findlocation.di.module

import android.app.Application
import android.content.Context
import com.example.findlocation.App
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val applicationContext: Context) {

     @Provides
     fun provideApplicationContext():Context = applicationContext
}