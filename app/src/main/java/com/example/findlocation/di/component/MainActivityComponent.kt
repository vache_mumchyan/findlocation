package com.example.findlocation.di.component

import com.example.findlocation.di.module.MainViewModelFactoryModule
import com.example.findlocation.di.scope.MainScope
import com.example.findlocation.ui.MainActivity
import dagger.Component

@MainScope
@Component(
    dependencies = [AppComponent::class], modules = [MainViewModelFactoryModule::class]
)
interface MainActivityComponent {

    fun inject(mainActivity: MainActivity)

}