package com.example.findlocation.di.component

import android.content.Context
import com.example.findlocation.data.repository.LocationRepository
import com.example.findlocation.data.repository.remote.RemoteRepository
import com.example.findlocation.di.module.AppModule
import com.example.findlocation.di.module.RemoteModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        RemoteModule::class
    ]
)
interface AppComponent {

    fun getContext() : Context

    fun getRemoteRepository(): RemoteRepository

    fun getLocationRepository(): LocationRepository

}