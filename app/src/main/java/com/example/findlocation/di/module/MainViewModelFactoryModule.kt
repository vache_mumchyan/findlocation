package com.example.findlocation.di.module

import androidx.lifecycle.ViewModelProvider
import com.example.findlocation.di.MainViewModelProviderFactory
import dagger.Binds
import dagger.Module

@Module
abstract class MainViewModelFactoryModule {

    @Binds
    abstract fun bindViewModelModelFactory(modelProviderProviderFactory: MainViewModelProviderFactory)
            : ViewModelProvider.Factory
}