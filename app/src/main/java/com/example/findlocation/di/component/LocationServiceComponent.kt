package com.example.findlocation.di.component

import com.example.findlocation.di.module.MainViewModelFactoryModule
import com.example.findlocation.di.scope.ServiceScope
import com.example.findlocation.services.LocationService
import com.example.findlocation.ui.MainActivity
import dagger.Component

@ServiceScope
@Component(dependencies = [AppComponent::class])
interface LocationServiceComponent {

    fun inject(locationService: LocationService)

}