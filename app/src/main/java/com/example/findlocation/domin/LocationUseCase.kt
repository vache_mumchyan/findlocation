package com.example.findlocation.domin

import com.example.findlocation.data.entity.Location
import com.example.findlocation.data.repository.LocationRepository
import com.example.findlocation.data.repository.remote.RemoteRepository
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class LocationUseCase @Inject constructor(
    private val locationRepository: LocationRepository,
    private val remoteRepository: RemoteRepository
) {
    fun execute(): Flowable<Location> =
        locationRepository.getLocation()
            .flatMap { location ->
                remoteRepository.setLocation(location)
                return@flatMap Single.just(location)
            }
            .onErrorResumeNext(Single.just(Location(0.0, 0.0)))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .repeatWhen { completed -> completed.delay(5, TimeUnit.SECONDS) }

}